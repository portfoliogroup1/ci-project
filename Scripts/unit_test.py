"""
Unit Test
Description: This script will unit test the code. It is fairly limited, but it is a good start.
Next project, I will start with this for each function.
"""
from art_asset_check import extension_check
from art_asset_check import dimension_name_check

def test_ext():
    """
    Test Extension Check
    Description:
        Test the extension function from art asset
    """
    allowed_file_extensions = [".jpg", ".png", ".gif"]
    test_dict = {
        "something.png": (True, ""),
        "something.gif": (True, ""),
        "something.jpg": (True, ""),
        "bad.file.fr": (False,
                        f"Wrong File Format: Only {' '.join(allowed_file_extensions)} Allowed")
    }
    for key, value in test_dict.items():
        assert extension_check(key) == value

def test_dimensions():
    """
    Test Dimension Name Check
    Description:
        Test the pixel size and the naming convention
    """
    assets_dict = {
        "graphics/bg-Abandoned_Store.png": (True, ""),
        "graphics/char-farmer.png": (True, ""),
        "graphics/wep-Wood_Bat.png": (True, ""),
        "graphics/bad_name.png": (False, "Wrong Nameing Convention"),
        "graphics/wep-bad_pixels.png": (False, "Wrong Pixel Dimensions")
    }
    for key, value in assets_dict.items():
        assert dimension_name_check(key) == value

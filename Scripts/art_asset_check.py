"""
Art Asset Check
Description: This script will get all new items added to the S3 bucket and then determine if
they pass a series of test to include:
    1. Correct file type
    2. Correct pixel size for asset type
        a. Character Sprites (CS) - 64 x 64px
        b. General Assests (GA) - no predetermined pixel size
        c. Background Assets (BA) - 1980 x 1020px
"""
import os
import sys
import json
import boto3
from PIL import Image


def connect_to_aws():
    """
    Connect To AWS
    Description:
        Helper function for conencting to AWS.
    Params:
        Access Key - AWS Access Key, grabbed from terminal
        Secret Key - AWS Secret Key, grabbed from terminal
        Bucket - AWS Bucket Name, grabbed from terminal
    Return:
        Client - AWS object for connecting/manipulating services
        Bucket - Bucket Name
    """
    access_key = sys.argv[1]
    secret_key = sys.argv[2]
    client = boto3.client('s3',
                           aws_access_key_id = access_key,
                           aws_secret_access_key = secret_key)
    bucket = sys.argv[3]
    return client, bucket


def check_staging_bucket(client, bucket):
    """
    Check Staging Bucket
    Description:
        Function for downloading items from AWS bucket into the Staging Folder. This folder goes
        away post run in pipeline
    Params:
        Client - AWS Client from Connect to AWS
        Bucket - AWS Bucket name
    Return:
        Staging_Dir - Directory containing items from S3 Bucket
    """
    staging_dir = os.path.join(os.getcwd(), "Staging")
    os.mkdir(staging_dir)
    result = client.list_objects_v2(Bucket=bucket, Prefix="Staging")
    keys = [obj['Key'] for obj in result['Contents']]
    for key in keys:
        print(key)
        if '.' in key:
            client.download_file(
                Bucket=bucket,
                Key=key,
                Filename=os.path.join(staging_dir, key[key.find('/')+1:])
            )
    return staging_dir



def sort_files(staging_dir, bucket):
    """
    Sort Files
    Description:
    Logical item that checks for
        Correct File Extension
        Pixel Size (Needs to be developed)
    Params:
        staging_dir - Folder containing assets from S3 Bucket
    Return:
        issue_dict - Dictionary of results to be used later in a notification function
    """
    issue_dict = {
                    "Bad Items": {},
                    "Good Items": []
    }
    s_3 = boto3.resource('s3')
    files = os.listdir(staging_dir)
    for file in files:
        ext_pass, ext_string = extension_check(file)
        # Kick bad items back
        if ext_pass:
            file_path = os.path.join(staging_dir, file)
            dim_name_pass, dim_name_string = dimension_name_check(file_path)
            if dim_name_pass:
                print(f"{file} passes all test.\nMoving to Utilization")
                issue_dict['Good Items'].append(file)
                copy_source = {
                    'Bucket': bucket,
                    'Key': f'Staging/{file}'
                }
                s_3.meta.client.copy(copy_source, bucket, f"Utilization/{file}")
                s_3.Object(bucket, f'Staging/{file}').delete()
            else:
                print(dim_name_string)
                issue_dict["Bad Items"][file] = dim_name_string
        else:
            print(ext_string)
            issue_dict["Bad Items"][file] = ext_string
    return issue_dict


def extension_check(asset):
    """
    Extension Check
    Description:
        Checks to ensure file extensions are appropriate
    Params:
        asset - The asset file path
    Return:
        boolean - True if the item passes the test
        string - Description for why the item failed
    """
    allowed_file_extensions = [".jpg", ".png", ".gif"]
    asset_extension = asset[asset.rfind('.'):]
    if asset_extension in allowed_file_extensions:
        return True, ""
    return False, f"Wrong File Format: Only {' '.join(allowed_file_extensions)} Allowed"


def dimension_name_check(asset):
    """
    Dimension Name Check
    Description:
        Checks the files are in the appropriate naming convention and that the pixel size is correct
    Params:
        asset - Asset file name
    Return:
        boolean - True if item passes all test
        string - Description of why the item failed
    """
    width, height = Image.open(asset).size
    # Make Configuration File (JSON Probably)
    prefix_dict = {
        "char-": [16, 32],
        "wep-": [64, 64],
        "bg-": [205, 232]
    }
    prefix = asset[asset.rfind('/') + 1: asset.rfind('-') + 1]
    if prefix in prefix_dict:
        if width == prefix_dict[prefix][0] and height == prefix_dict[prefix][1]:
            return True, ""
        return False, "Wrong Pixel Dimensions"
    return False, "Wrong Nameing Convention"

def save_dict(issue_dict):
    """
    Save Dict
    Description:
        Converts the dictionary of item evaluations into a json file
    Params:
        issue_dict - created from the check staging bucket script. Contains the acceptable and
        failed assets along with their description
    Return
        n/a
    """
    notification_dir = os.path.join(os.getcwd(), "notification")
    os.mkdir(notification_dir)
    li_dir = [notification_dir, "discord.json"]
    with open(os.path.join(*li_dir), "w") as outfile:
        json.dump(issue_dict, outfile)


if __name__ == "__main__":
    client, bucket = connect_to_aws()
    staging_dir = check_staging_bucket(client, bucket)
    issue_dict = sort_files(staging_dir, bucket)
    save_dict(issue_dict)

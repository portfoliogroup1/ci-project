"""
Discord Hook
Description: This script will take in the notification json file generated from prior processes
It will then parse this data into an appropriate message which is then sent to discord.
"""
import json
import os
import sys
import datetime
import requests

def get_data(file_location):
    """
    Get Data
    Description:
        Parses the json notification file
    Params: 
        file_location - Location of json file
    Return:
        content - parsed message
    """
    with open(file_location) as json_file:
        notification_dict = json.load(json_file)
    content = "Nothing Kicked Back or Ready For Utilization...thats sus..."
    if notification_dict["Bad Items"]:
        bad_items = "\n***Rework Items***"
        for key, value in notification_dict["Bad Items"].items():
            bad_items += f"\nFile: {key} | {value}"
        content = bad_items
    if notification_dict["Good Items"]:
        good_items = "\n***Items Ready for Utilization***"
        for i in notification_dict["Good Items"]:
            good_items += f"\n{i}"
        if notification_dict["Bad Items"]:
            content += good_items
        else:
            content = good_items
    return content

def discord_notification(content):
    """
    Discord Notificaiton
    Description:
        Post the message in discord
    Params:
        content - parsed message from prior function
    Return:
        n/a
    """
    #load_dotenv()
    #fix env item
    url = "https://discord.com/api/webhooks/989301934789910528/1BzRsLQtIKtOXZQsb8VM7SBDF3Q7dby1chhbteDw3qx1xF2SaKe7og60hYvt_lpy_vls"
    print(url)
    now = datetime.datetime.now()
    payload = json.dumps({
    "content": f"***Pipeline Processed @ {now.year}-{now.month}-{now.day} {now.hour}:{now.minute}\
        ***\n{content}"
    })

    headers = {
        'Content-Type': 'application/json',
        'Cookie': sys.argv[1]
    }

    requests.request("POST", url, headers=headers, data=payload)

file_path = os.path.join(os.getcwd(), "notification/discord.json")
contents = get_data(file_path)
discord_notification(contents)

# Table of Contents
1. [Overview](#Overview)
1. [Legend](#legend)
2. [Asset Creation & Verification](#process-asset-creation-verification)
3. [Background Task](#background-task)
4. [How-Tos](#how-tos)
    - [AWS S3 Bucket](#aws-s3-buckets)
    - [AWS Lambda](#aws-lambda-function)
    - [GitLab CICD](#gitlab-cicd)
    - [Gitlab Pipeline Schedule](#gitlab-pipeline-schedule)
    - [Runners](#runners-local)

# Overview

# Legend
- [ ] In-Progress
- [x] Complete
``` diff
- Item tried and failed
+ Item tried and was a great success
```
# Process: Asset Creation & Verification
### This process will recieve pushes of visual assets to be utilized by the game dev.
1. Create assets in Adobe Illustrator or Aseprite
2. Move finished assets to an AWS S3 Staging Bucket
3. Trigger in AWS S3 Bucket notifies AWS Lambda
6. AWS Lambda Script schedules the GitLab CICD Pipeline via the Gitlab API
    - ISSUE [SOLVED]: Multiple file puts starts multiple pipelines
    - Possible Solutions:
        ``` diff
        - [x] Configure Trigger - AWS Lambda doesn't seem to have the functionality
        - [x] Configure GitLab to throttle API hits - Need admin access
        + [x] Write a script to check if a pipeline is scheduled, if it is then dont start another one, if it isnt, schedule one
        + This kind of fixes it, but multiple small files still may result in multiple runs
        ```
4. Pipeline runs on a schedule and checks for:
    - [x] Correct File Type - .jpg .png .gif
    - [x] Correct Naming Convention - char- wep- bg-
    - [x] Correct Pixel Sizing - relative to file name
5.  Items that pass the above test are moved to a utilizaiton bucket for game devs to ingest into their game 
    - Assets that fail the test stay in staging folder and are marked for rework
6. Notifications are sent via Discord API to the developers and asset creators

## Background Task
This process includes some items that are not directly related to the above process such as:
- Setting up AWS Account
    - IAM Roles
    - Creation of a bucket
    - Creating the layer for the AWS Lambda
- Creating a custom runner
    - Create a linux virtual machine in Oracle VM Virtual Box
    - Creating snap shots
    - Installing dependencies
    - Registering runner
    - Inevitable fixing the virtual box

# How-Tos
## AWS S3 Buckets
Creating a bucket is relatively easy. It can be done through the following process:
1. Open your AWS Account
2. Type "S3" into the searchbar
3. Select the S3 icon from the drop downs
4. On the right side of the screen select "Create Bucket"
5. Name your bucket
6. Select the appropriate region
7. Leave everything else as the default
8. Select "Create Bucket" at the bottom of the screen

You now have a functioning bucket and can select it in your window. 

Folders can be created here with their unique names. I made one called Staging and another called Utilization.

You can then drag and drop items into these buckets as you see fit.
### AWS S3 Bucket Event Notifications
Event Notifications are a very handy item in the S3 buckets. It essentially operates like this, "if something happens, send a message to this location"
I used this in the following way, "If an item is put into the staging bucket, send a message to AWS S3 Lambda named 's3Put'"

These event notificaitons can be added in the following way:
1. With your bucket open select the "Properties" tab
2. Scroll down to "Event Notificaitons" and select "Create event notificaiton"
3. Give your notification a name
4. Add a prefix if you'd like  
    - Prefixs are the folder name that you want to watch
    - Example: I want to watch the staging folder so I added a prefix of "Staging/"
5. Read the options for "Event Types" and select the best one for you. 
    - I selected the "Put" object which will send a message anytime any item is put into the folder.
    - NOTE: This is not ideal for my process, I would like for one message to be sent when a multi-item upload is complete. However, this is the best option I had access to.
6. Scroll down to destination and select "Lambda Function"
7. If your lambda function is already made, select it from the drop down.
    - If not then just wait until you finish the next how to. 
8. Select "Save Changes"
## AWS Lambda Function
The AWS Lambda service really handy for communication between AWS services and outside items. It enables you to enter simple code that can tip off other processes.
To create an AWS Lambda do the following:
1. Select "Create Function" on the AWS Lambda page.
2. Select "Author From Scratch"
3. Name your function
4. Select your runtime environment, I selected "Python 3.9"
5. Leave everything else the same and select "Create Function"

You now have a functioning lambda function, it is relatively blank, but it can be very useful.
At this point you can go back to the AWS s3 Bucket Event and add this function to your Event Notificaiton. 
### AWS Lambda Layer
This was really annoying when I first started trying to do this. Don't go through the pain of making your own right now, just do the following:
1. Scroll down to the "Layers" tab.
2. Select "Add A Layer" 
3. Select "AWS Layers"
4. From the drop down select "AWSDataWrangler-version"
    - This very well may be overkill, but this one worked for the things I was trying to do.
    - If your good at setting up layers in AWS, you can make your own, I wanted to keep this simple.
5. Select "Add"

Now you can actually run code with some external modules.

The AWS Lambda function is now your oyster to mess with. I'd recommend testing small changes. Here is an [example](https://docs.aws.amazon.com/AmazonS3/latest/userguide/notification-content-structure.html) of the event json file from the S3 Bucket. 

NOTE: Do not remove the Lambda_Handler function in the starter code. Use that as your "main" function as this is called when the Lambda is triggered.

NOTE: If you dont select "Deploy" you code changes will not be tested. Spent a little too long figuring that one out. 
## Gitlab CICD
This was the desired learning item of this project. To learn the basics of CICD in a practical way. The AWS items were peripheral, but neccessary. The more I learn about CICD, the more I see it as the connective tissue between services. It enables you to expedite and automate alot of the boring processes that regularly get forgotten about.

### Welcome to Yaml
The code component of CICD is the .gitlab-ci.yml file. This item is the instructions for all the other processes in this project. Think of the yaml as the cordinator of a factory.
For this how-to I will walk through the basics of how I set mine up, and then will point at something to experiment with.

To begin, lets make the yaml file at our base directory you can do this by either clicking the "Add CI/CD Configuration" button at your repo head or making a file at the root of your project titled exactly ".gitlab-cd.yml". 



## Gitlab Pipeline Schedule
### Manual
Gitlab pipeline schedules are a really nice way to run your pipeline once for multiple changes to your base data. With the free version of Gitlab, you can schedule runs at the top of every hour. There are other nice options with the paid version, such as a way to not DDOS yourself if you send too many schedules at once, but we are trying to keep this to minimal cost. If your paying for Gitlab then you can probably schedule more often, this is also quite taxing on your rescources, and may cause you to incur cost. If you want to dodge that, you can set up a custom runner on your local machine or a virtual machine, I will show you how to do this later. For now, I believe the free version comes with 400 minutes of free run time per month. You shouldn't be close to that for right now, but pay attention to it if you're working on multiple projects.

To schedule a pipeline manually, do the following:
1. Navigate to your gitlab project
2. Select "CI/CD"
3. Select "Schedules"
4. Select "New Schedule"
5. Add a description as your see fit
6. Add an interval pattern in Cron
    - If you dont know what Cron is, check it out [here](https://en.wikipedia.org/wiki/Cron)
    - In short, schedule it like this minute(0-59) hour(0-23) day(1-31) month(1-12) day of the week(0-6 Sunday to Saturday)
7. Select your timezone and leave the rest as defaults
8. Select "Save Pipeline Schedule"
### Webhook
Now that we have done the manual way through gitlab, lets explore the webhook version. You will put this into your lambda function later. 

Download [Postman](https://www.postman.com/) to do testing. Postman is awesome once you learn how to use it. I will do my best to show you how to use it so you don't get stuck in tutorial heck like I was for a few weeks. 

Open Postman on your desktop, along with the [Gitlab API documentation](https://docs.gitlab.com/ee/api/). The docs look a little intimidating at first, but you'll be a doc utilizing son of a gun in no time. 

### Basics of the Docs
To get started, lets get our current projects information. Go to the [Project Information](https://docs.gitlab.com/ee/api/projects.html#get-single-project) section and lets use this information to make our first webhook.

The below information looks pretty inocent, but might slow you down initially.
```
    GET /projects/:id
```
Lets break this down:
- GET - This is the request type. GET grabs data from the requesting service. Think of this as telling the API what your intentions are. "I am looking to get some information from your service."
- /projects - This is a discintion within the API, like a sub process. This of this as the department you are reaching out to. Now we are saying, "I am looking for some information from your projects service."
- :id - Any time you see a ":" proceeding something in an API, it means this is a variable that you need to provide a value for. You can find your projects id at the projects base page, and it should be under your projects name in the top center of the screen. You will have an option to copy this information there. Now we are saying the following to the API, "I am requesting information on id XXXXXXXXX from your projects service department."

Now we have the basics of our request aside from the leading api declaration. Assuming the API is still in version 4, you will begin your API request with: https://gitlab.com/api/v4/

So our full request will look like this:
```
    GET https://gitlab.com/api/v4/projects/your_project_id
```
### First Request in Postman
Now, lets enter this into Postman.
1. Open Postman and select "Collections" on the left side of the screen
2. Select "New"
3. Select "HTTP Request"
    - You now have a blank request on your screen
4. Ensure the drop down for request type, is "GET"
5. Copy and Paste the initial declaration into the request URL
    - https://gitlab.com/api/v4/projects/
6. We will now add the id by using the key value items below.
7. Type "id" into the key column
8. Copy and Paste your id into the value column

Your full request should look like this
```
    GET https://gitlab.com/api/v4/projects/?id=your_project_id
```
9. Click "Send"
    - You should observe a body of information being returned to you. You may notice this information is a little short. We will now pass on our Gitlab Credentials to the project so we can access more information.
### Access Token
To get your access token:
1. In Gitlab, click your portrait in the top right and select "Preferences"
2. Select "Access Tokens" on the left side
3. Name your token
4. You can give it an expiration date, if you dont it will be eternal
5. Select the "API" check box
6. Select "Create Personal Access Token"
7. Copy and Paste this code somewhere safe because you will not get access to it again. 

Now lets enter the access token into our request.
1. In our postman Get request, select the tab "Authorization"
2. In the "Type" dropdown list, select "Bearer Token"
3. In the token field, copy and paste your Gitlab Token
4. Now send your request again.
    - NOTE: Your request message will not change
    - You should now observe much more information coming back about your project
### Exporting to Code
Postman has a nice feature for exporting the process you just did to code. Once you have a functioning process, go to the right side of your screen and selec the icon that looks like "</>". This will give you lots of options to export your request as. Scroll down and select "Python - Requests". This will generate a code snippet that you can copy and paste into your code.
- NOTE: This will display your token in plain text, be sure to port this to a .env variable or a Gitlab variable before posting your code. 

## Runners (Local)
I am by no means an expert or even intermediate in the following topic. Configuring the Linux Virtual Machine and its subsequent runner kicked my butt, and is still giving my problems. I think it is an issue with my desktop. So, if you're using an ASUS machine and your CPUs are regularly getting hung when uploading artifacts. Let me know how you fixed it because mine still crashes regularly. 

The real pro-tip in all of this is to use an AWS EC2 instance as a runner. It is so much easier than what I had initially tried to do. [Here](https://www.youtube.com/watch?v=HGJWMTNeYqI) is the video tutorial I used as a guide. 

### Oracle VM Virtual Box
Virtual Box is the item I used to make virtual machines because it was the most common and was supposed to be the easiest to use. You can find the download for it [here](https://www.virtualbox.org/wiki/Downloads).

While this is downloading, you will need to get your ISO for Linux, that can be found [here.](https://ubuntu.com/download) I put this download in the same folder that I was going to put my VM's storge. 

Now that those are downloaded, you can make your first VM. With Oracle VM Virtual Box open, do the following. 
1. Select "New" in the top middle of your screen.
2. Name your virtual box, mine is called "Gitlab-Runner"
3. Select the machine folder, anywhere you have room should be fine
4. Type, select the drop down and choose "Linux"
5. Version, select "Ubuntu (64-bit)" assuming you downloaded a 64bit version of Linux
6. Select next
7. For Memory Size, if your computer can handle it, give the VM atleast 4GBs or 4096MBs
8. Select Next
9. For Hard Disk, select "Create a virtual hard disk now"
10. Select "VDI", then next
11. Select "Dynamically Allocated", then next
    - Dynamically Allocated memory means the VM will take up as little space as possible, but as you add more items the size will balloon up to a max that you determine. 
12. For "File Location and Size" select a location that has sufficient memory, and for size, I would recomend atleast 100GBs.
    - Remember, because it is dynamic it will not pre-allocate 100GBs, it will only cap at 100GBs.
    - I messed this up the first time, and only gave it 10Gbs. It capped out and I was unable to change it and had to start over.
13. Select "Create"
    - Your VM is technically there now, but it may not be usable yet. We have to change a few more settings to get it functional. 
14. Select your new VM and select "Settings" in the top middle of the screen
15. In this new screen, select "System" and then "Processor"
16. From this menu, I would recomend allocating atleast 2 CPUs to the VM. 
    - I don't understand why, but when I only have 1 CPU I cannot launch my VM 
17. Select "Storage" and then select the empty item under  "Controller" IDE
18. Select the Blue Disk with a small black arrow, select the "Choose a Disk File" option
19. Navigate to your '.iso' file you downloaded from Ubuntu
20. Click "Okay"
21. Select "Start" with your VM selected
    - The VM will now start up, this may take anywhere from 5 to 60 minutes depending on your system.

Congratz, you should have your VM now.

### Pro-Tip Snapshots
Take snapshots of your VM frequently. I learned this the hard way. After you have confirmed a few configuration steps are working apporpraitly, do a quick screen shot. If you mess up the next step or something goes weird with your next step you can reload from that point quickly (less than 1 minute when I have tried it)

You can take these screen shots from the Virtual Box home page. 
1. Highlight your VM
2. Select the item that looks like a list icon
3. Select screen shots
4. Click the "Take" button at the top middle of the screen. 

You can restore to one of your snapshots by:
1. Make sure your VM is shut down
2. Go to the snapshot page on Virtual Boxes homepage
3. Select the snapshot you would like
4. Click restore 

Now start your VM again and you will be back where you started.

### Supporting Installs
With your running VM you will now be able to register it as a runner. The VM is basically blank right now and you will have to install all dependencies into your VM. This is fairly straight forward to do and there are lots of great tutorials out there. This how to will walk you through some of the basics that will enable you to customize your runner. 

We will be installing Python, Pip, and Curl.

With your terminal open in your VM:

First, lets get curl.
1. Type "sudo apt update" and select enter and "sudo apt upgrade" and select enter
    - This will get the latest version of Ubuntu.
2. Now enter "sudo apt install curl"

Congratz you now have Curl

Now lets get Python
1. Type "sudo apt-get install python3" and hit enter
    - This will get you the latest version of Python
2. To check that it is install type "python3 --version" 
    - You should see that python is installed
    - If you get an error something went wrong and it was not installed

You will also need pip to install all your dependencies.
1. Type "sudo apt install python3-pip" and hit enter
2. To confirm type "pip3 --version"
    - If you get a version number you did it right.
Now with the command "pip3 install *libary*" you can install any package you may need.

### Gitlab Runner Install/Config
Luckily gitlab makes this really easy because they give you the step by step instructions on your page. It may be helpful to sign into your gitlab account within your VM so you can copy and paste the commands. You can do this within your VM if you download some additional tools. The commands are pretty long so either of those options would be wise. 

The walkthrough from gitlab can be found in your projects page.
1. Side Menu -> Settings -> CI/CD
2. Then expand runners
3. Under "Specific Runners" select "Show runner installation instructions"

You can copy and paste these instructions into your terminal to get going quickly. 

The only tricky part is with registering your runner. This menu does not list your token, but it can be found by closing the menu, and copying the token in your specific runner's menu.

After this the terminal will ask you for some specifics, for the most part you can put whatever you like, except for the tags and runner type.

The tag is what you will use to reference in your yaml file.
The runner type should be "shell" for this walkthrough.

Your runner should now appear in your specific runner section of your gitlab page.

You can call the specific runner in your yaml by replacing the image with:
```
tag: 
    - your-runners-tag
```
